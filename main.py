import discord
import os
from dotenv import load_dotenv

#Get home directory
home = os.getenv("HOME")

# DotEnv stuff
path = f'{home}/Documents/Programming/Python/Discord/.env'  # Path to .env
load_dotenv(dotenv_path=path, verbose=True)  # idk what this is

# Main Code
class MyClient(discord.Client):
    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')

    async def on_message(self, message):
        # we do not want the bot to reply to itself
        if message.author.id == self.user.id:
            return

        if message.content.startswith('!ping'):
            await message.channel.send('Pong!')

        if message.content.startswith('!verify'):
            verified = "testRole"             # change testRole to the verified role in your server
            Member = message.author
            role = discord.utils.get(message.guild.roles, name=verified)
            await Member.add_roles(role)
            await message.channel.send('Verified!')


client = MyClient()
client.run(os.getenv("TOKEN"))
